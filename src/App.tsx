import React from 'react';
import './App.css';
import { Kab } from './components/Kab';

function App() {
  return (
    <div className="App">
       <Kab/>
    </div>
  );
}

export default App;
